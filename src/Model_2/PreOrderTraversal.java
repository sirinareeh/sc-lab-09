package Model_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import Interface_2.Traversal;

public class PreOrderTraversal implements Traversal {

	@Override
	public List<String> traverse(Node node) {
		List<String> list = new ArrayList<String>();
        if(node == null)
            return list;
 
        Stack<Node> stack = new Stack<Node>();
        stack.push(node);
 
        while(!stack.empty()){
            Node n = stack.pop();
            list.add(n.getvalue());
 
            if(n.getright() != null){
                stack.push(n.getright());
            }
            if(n.getleft() != null){
                stack.push(n.getleft());
            }
 
        }
        return list;
	}
	

}
