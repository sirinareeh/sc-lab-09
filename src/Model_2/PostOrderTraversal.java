package Model_2;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import Interface_2.Traversal;

public class PostOrderTraversal implements Traversal{

	@Override
	public List<String> traverse(Node node) {
		List<String> list = new ArrayList<String>();
		 
        if(node == null)
            return list; 
 
        Stack<Node> stack = new Stack<Node>();
        stack.push(node);
 
        Node prev = null;
        while(!stack.empty()){
            Node curr = stack.peek();
 
            if(prev == null || prev.getleft() == curr || prev.getright() == curr){
                if(curr.getleft() != null){
                    stack.push(curr.getleft());
                }
                else if(curr.getright() != null){
                    stack.push(curr.getright());
                }
                else{
                    stack.pop();
                    list.add(curr.getvalue());
                }
            }
            else if(curr.getleft() == prev){
                if(curr.getright() != null){
                    stack.push(curr.getright());
                }
                else{
                    stack.pop();
                    list.add(curr.getvalue());
                }
 
            }
            else if(curr.getright() == prev){
                stack.pop();
                list.add(curr.getvalue());
            }
 
            prev = curr;
        }
 
        return list;
	}

}
