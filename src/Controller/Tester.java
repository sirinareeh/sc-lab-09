package Controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.ProfitComparator;
import Model.Company;
import Model.Person;
import Model.Product;
import Model.TaxComparator;
import Interface.Taxable;

public class Tester {


	public static void main(String[] args) {
		Tester tester = new Tester();
		tester.testQuestion1a();

	}
	public void testQuestion1a(){
		List<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Nadech", 182, 180000));
		persons.add(new Person("Siwon", 183, 620000));
		persons.add(new Person("Mario", 180, 55000));
		
		System.out.println("------ Before sort with Yearly income ------");
		
		for (Person c : persons) {
			System.out.println(c);
		}
		
		Collections.sort(persons);
		
		System.out.println("------ After sort with Yearly income -------");
		for (Person c : persons) {
			System.out.println(c);
		}
		
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("Nikon d7200", 55000));
		products.add(new Product("Fuji x-m1",19900));
		products.add(new Product("Canon 70d",31500));
		
		
		System.out.println("\n"+"------ Before sort with Price ------");
		for (Product c : products) {
			System.out.println(c);
		}
		
		Collections.sort(products);
		System.out.println("------ After sort with Price -------");
		for (Product c : products) {
			System.out.println(c);
		}
		
		List<Company> companys = new ArrayList<Company>();
		companys.add(new Company("Nikon", 360000, 20000));
		companys.add(new Company("Fuji", 40000, 5300));
		companys.add(new Company("Canon", 680000, 60000));
		
		System.out.println("\n"+"------ Before sort  ------");
		for (Company c : companys) {
			System.out.println(c);
		}
		
		Collections.sort(companys, new EarningComparator());
		System.out.println("------ After sort with income -------");
		for (Company c : companys) {
			System.out.println(c);
		}
		
		Collections.sort(companys, new ExpenseComparator());
		System.out.println("------ After sort with expense -------");
		for (Company c : companys) {
			System.out.println(c);
		}
		
		Collections.sort(companys, new ProfitComparator());
		System.out.println("------ After sort with profit -------");
		for (Company c : companys) {
			System.out.println(c);
		}
		
		List<Taxable> taxperson = new ArrayList<Taxable>();
		ArrayList<Taxable> taxcompany = new ArrayList<Taxable>();
		ArrayList<Taxable> taxproduct = new ArrayList<Taxable>();
		ArrayList<Taxable> taxsum = new ArrayList<Taxable>();
		
		taxperson.add(new Person("Nadech", 182, 180000));
		taxperson.add(new Person("Siwon", 183, 620000));
		taxperson.add(new Person("Mario", 180, 55000));
		
		System.out.println("\n------ Before sort with tax ------");
		
		for (Taxable c : taxperson ) {
			System.out.println(c);
		}
		
		Collections.sort(taxperson, new TaxComparator());
		
		System.out.println("------ After sort with tax -------");
		for (Taxable c : taxperson) {
			System.out.println(c);
		}
		
		taxproduct.add(new Product("Nikon d7200", 55000));
		taxproduct.add(new Product("Fuji x-m1",19900));
		taxproduct.add(new Product("Canon 70d",31500));
		
		System.out.println("\n------ Before sort with tax ------");
		
		for (Taxable c : taxproduct) {
			System.out.println(c);
		}
		
		Collections.sort(taxproduct, new TaxComparator());
		System.out.println("------ After sort with tax -------");
		for (Taxable c : taxproduct) {
			System.out.println(c);
		}
		
		
		taxcompany.add(new Company("Nikon", 360000, 20000));
		taxcompany.add(new Company("Fuji", 40000, 5300));
		taxcompany.add(new Company("Canon", 680000, 60000));
		
		System.out.println("\n------ Before sort with tax ------");
		
		for (Taxable c : taxcompany) {
			System.out.println(c);
		}
		
		Collections.sort(taxcompany, new TaxComparator());
		System.out.println("------ After sort with tax -------");
		for (Taxable c : taxcompany) {
			System.out.println(c);
		}
		
		
		taxsum.add(new Person("Nadech", 182, 180000));
		taxsum.add(new Person("Mario", 180, 55000));
		taxsum.add(new Company("Fuji", 40000, 5300));
		taxsum.add(new Company("Nikon", 360000, 20000));
		taxsum.add(new Product("Fuji x-m1",19900));
		
		System.out.println("\n------ Before sort with tax ------");
		for (Taxable c : taxsum) {
			System.out.println(c);
		}
		
		Collections.sort(taxsum, new TaxComparator());
		System.out.println("------ After sort with tax -------");
		for (Taxable c : taxsum) {
			System.out.println(c);
		}
	}

}
