package Model;

import java.util.Comparator;

import Interface.Taxable;

public class TaxComparator implements  Comparator {

	@Override
	public int compare(Object c1, Object c2) {
		double tax1 = ((Taxable)c1).getTax();
		double tax2 = ((Taxable)c2).getTax();
		if (tax1 > tax2) return 1;
		if (tax1 < tax2) return -1;
		return 0;
		
	}

}
