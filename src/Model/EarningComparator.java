package Model;

import java.util.Comparator;

public class EarningComparator implements Comparator {

	@Override
	public int compare(Object c1, Object c2) {
		// TODO Auto-generated method stub
		double income1 = ((Company)c1).getIncome();
		double income2 = ((Company)c2).getIncome();
		if (income1 > income2) return 1;
		if (income1 < income2) return -1;
		return 0;
	}

}
