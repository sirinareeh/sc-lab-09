package Model;

import java.util.Comparator;

public class ProfitComparator implements Comparator{

	@Override
	public int compare(Object c1, Object c2) {
		// TODO Auto-generated method stub
		double profit1 = ((Company)c1).getProfit();
		double profit2 = ((Company)c2).getProfit();
		if (profit1 > profit2) return 1;
		if (profit1 < profit2) return -1;
		return 0;
	}

}
