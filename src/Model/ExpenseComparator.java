package Model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator {

	@Override
	public int compare(Object c1, Object c2) {
		// TODO Auto-generated method stub
		double expenses1 = ((Company)c1).getExpenses();
		double expenses2 = ((Company)c2).getExpenses();
		if (expenses1 > expenses2) return 1;
		if (expenses1 < expenses2) return -1;
		return 0;
	}

}
