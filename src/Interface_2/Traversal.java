package Interface_2;
import java.util.List;
import Model_2.Node;

public interface Traversal {
	
	List<String> traverse(Node node);
}
