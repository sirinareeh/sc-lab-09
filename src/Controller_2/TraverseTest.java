package Controller_2;

import Model_2.InOrderTraversal;
import Model_2.Node;
import Model_2.PostOrderTraversal;
import Model_2.PreOrderTraversal;
import Model_2.ReportConsole;

public class TraverseTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TraverseTest  tester = new TraverseTest ();
		tester.testTraverse();
	}

	private void testTraverse() {
		
		Node n9 = new Node("H",null,null);
		Node n8 = new Node("E",null,null);
		Node n7 = new Node("C",null,null);
		Node n6 = new Node("I",n9,null);
		Node n5 = new Node("D",n7,n8);
		Node n4 = new Node("A",null,null);
		Node n3 = new Node("G",null,n6);
		Node n2 = new Node("B",n4,n5);
		Node n1 = new Node ("F",n2,n3);
		
		PreOrderTraversal pre = new PreOrderTraversal();
		pre.traverse(n1);
		ReportConsole.display(n1,pre);
		
		InOrderTraversal in = new InOrderTraversal();
		in.traverse(n1);
		ReportConsole.display(n1,in);
		
		PostOrderTraversal post = new PostOrderTraversal();
		in.traverse(n1);
		ReportConsole.display(n1,post);
		
	}

}
